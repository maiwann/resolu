---
title: Résolu
---

# Réseaux éthiques et solutions ouvertes pour libérer vos usages

[![resolu](./img/cover_small.jpg){: #cover srcset="/img/cover_small.jpg 480w, /img/cover_big.jpg 800w, /img/cover.jpg 2x" sizes="(max-width: 480px) 440px, 800px"}](https://framabook.org/resolu/){target=_blank  rel=noopener}

Construire un monde meilleur n'est possible qu'avec les outils qui
nous en donnent la liberté
{: .slogan-subtitle }

Agir pour l'Économie Sociale et Solidaire demande beaucoup
d'organisation, de communication et de collaboration. Promouvoir un
modèle de production alternatif fondé sur la solidarité et le partage
est un travail collectif qui ne peut se passer d'outils numériques pour
être efficace. Bien souvent, ces outils sont choisis par défaut, et ne
correspondent ni aux besoins ni aux valeurs des organisations qui les
utilisent.

L'objectif de \[Résolu\] est d'accompagner votre
organisation vers l'adoption de solutions alternatives libres. Quelques
outils vous sont présentés sous forme de fiches ; ils ont été
sélectionnés pour leur pertinence vis-à-vis des pratiques numériques des
collectifs. Pour mieux comprendre les enjeux de cette transition pour
votre organisation, vous trouverez également des fiches mettant en
lumière la cohérence entre les valeurs libristes et celles de
l'Économie Sociale et Solidaire. Nous espérons qu'à la lecture de cet
ouvrage, vous serez convaincu⋅e de vouloir garder le contrôle de vos
usages numériques en faisant le choix des logiciels libres. Soyons
résolu⋅e⋅s à soutenir ensemble un modèle de société fondé sur
l'accessibilité, la contribution, l'ouverture et la solidarité.

## Mode d'emploi

Les version PDF (lecture et impression) ainsi que les sources graphiques et *Open Document Format* de ce manuel sont mises à disposition de tous sur [Framabook](https://framabook.org/resolu/).
{: .encart }

Dans le respect de la licence libre mentionnée ci-dessous, vous pouvez utiliser tous ces contenus, y compris le présent site. Les fiches composant ce manuel étant indépendantes les unes des autres, vous pouvez les imprimer ou les éditer à part et les diffuser selon vos besoins. N'oubliez pas de mentionner la licence et les auteurs, y compris en cas de modification lorsque d'autres auteurs s'ajoutent à la liste.

## Version 1.0

### Réalisation (2020)

[![framasoft_logo](./img/framasoft_logo.png)](https://framasoft.org/)
[![picasoft_logo](./img/picasoft_logo.png)](https://picasoft.net/)
[![cemea_logo](./img/cemea_logo.jpg)](http://www.cemea.asso.fr/)
{: .home-realisation-logos}

<br>
Avec le soutien de :

[![fondation_free_logo](./img/fondation_free_logo.png)](https://www.fondation-free.fr/)
{: .home-realisation-logos}

### Co-auteur·rice⋅s, par ordre alphabétique du prénom

Audrey Guélou, Christophe Masutti, Pascal Gascoin, Rémi Uro, Stéphane
Crozat

### Licence

[![Licence Creative
Commons](./img/by-sa.png)](http://creativecommons.org/licenses/by-sa/3.0/)

Ce manuel est mis à disposition selon les termes de la [Licence
Creative Commons Attribution - Partage dans les Mêmes Conditions 3.0
International](http://creativecommons.org/licenses/by-sa/3.0/).

### Conception graphique 

[Odigi.eu](http://www.odigi.eu/)

Polices de caractères utilisées : Liberation, TeX Gyre Adventor

### Crédits des illustrations

Couverture : [Shane Rounce](https://unsplash.com/@shanerounce)
sur
[Unsplash](https://unsplash.com/s/photos/team)

Fiche « Zourit » : [Stephanie Klepacki](https://unsplash.com/@sklepacki)
sur
[Unsplash](https://unsplash.com/s/photos/octopus)

Fiche « Confidentialité » :
[Antonella Brugnola](https://unsplash.com/@ziaantonella)
sur
[Unsplash](https://unsplash.com/s/photos/privacy)

Fiche « Organiser » : [Jason Leung](https://unsplash.com/@ninjason)
sur
[Unsplash](https://unsplash.com/s/photos/organize)

Fiche « Conduite du changement » : [Javier Allegue Barros](https://unsplash.com/@soymeraki)
sur
[Unsplash](https://unsplash.com/s/photos/sign-direction)

Fiche « Communs numériques » : [Bekir Dönmez](https://unsplash.com/@bekirdonmeez)
sur
[Unsplash](https://unsplash.com/s/photos/team)

---

[https://framabook.org](https://framabook.org/)
