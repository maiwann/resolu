---
title: Collaborer
---

Pour travailler en équipe, il faut utiliser des outils numériques
adaptés
{: .slogan-subtitle }

La collaboration est indispensable au sein de toute structure. Il existe
de nombreux outils numériques pour travailler ensemble. Voyons quelques
réflexions à aborder au préalable afin de décider quels outils utiliser.

## Collaborer, c'est participer à une œuvre avec d'autres personnes

Mobiliser ses collaborateur⋅rice⋅s efficacement pour réaliser un travail
collectif est souhaitable dans bien des cas, mais ce n'est pas toujours
aussi facile qu'on le voudrait\... Les différents emplois du temps,
l'éloignement physique, les contraintes de circulation de l'information
et tant d'autres raisons peuvent rendre la collaboration compliquée.
Heureusement, des solutions existent pour (tenter de) résoudre ces
problèmes ! Cependant, toutes ne sont pas forcément adaptées aux usages
et aux habitudes, et certains outils qui semblent pratiques au début
peuvent devenir contraignants. C'est pourquoi il est important de
**maîtriser les outils** : pas seulement savoir s'en
servir, mais bien en être maître, c'est-à-dire ne pas être dépendant
d'acteurs qui décideraient unilatéralement de l'évolution ou de l'arrêt
d'un outil.

Les fiches **Collaborer** présentent en détail
quelques solutions qui permettent de répondre aux besoins de la
collaboration et dont il est possible de **conserver le
contrôle**.
{: .encart }

## Une question de confiance

Utiliser un service en ligne (on dit aussi un service cloud), revient à
stocker ses informations sur **l'ordinateur de quelqu'un⋅e
d'autre**. Il est donc nécessaire d'avoir confiance
dans cet autre acteur et d'en connaître les motivations. Par exemple,
s'il⋅elle a intérêt à ce que vous adoptiez une certaine façon de
travailler, il⋅elle va pouvoir modifier ses outils pour vous y pousser.

Vous faut-il pour autant travailler seul⋅e, en stockant vos fichiers
uniquement sur votre ordinateur, en local ? Heureusement non : vous
pouvez héberger vos fichiers et profiter des services de quelqu'un⋅e en
qui vous avez **confiance**.

Par exemple, certaines associations ou entreprises s'engagent avec une
charte de confiance et proposent des solutions basées sur Nextcloud, une
suite d'outils permettant aussi bien le stockage et le partage de
documents que l'édition collaborative ou encore la gestion de calendrier
[Nextcloud](nextcloud.md). En somme, vous devez trouver un
tiers de confiance et évaluer sa fiabilité ou bien créer vous même votre
service en ligne.
{: .encart }

## Partager, oui, mais partager bien

Lorsqu'on **partage** des documents, il est important
que tou⋅te⋅s les collaborateur⋅rice⋅s puissent les consulter et les
modifier dans les mêmes conditions. Les formats ouverts sont sous
licence libre, ils peuvent être interprétés et modifiés avec une
majorité de logiciels (libres ou non libres). On appelle cela
**l'interopérabilité**. Celle-ci est d'ailleurs décrite
dans un document présentant un ensemble de normes et bonnes pratiques
communes aux administrations publiques françaises dans le domaine
informatique : le référentiel général d'interopérabilité (RGI).

Le format OpenDocument est un exemple de format ouvert pour toutes les
applications de bureautique (.odt pour le texte, .ods pour les feuilles
de calcul...)

## Réunions et prise de rendez-vous

Des outils comme Skype ou Doodle sont largement utilisés pour organiser
des réunions à distance. Or, ces services nous rendent
**dépendant⋅e⋅s** d'entreprises dont les intérêts ne
sont pas forcément les nôtres. Skype est par exemple devenu bien moins
fluide dans sa version gratuite depuis son rachat par Microsoft, et
Doodle a décidé du jour au lendemain de devenir payant pour un certain
nombre de fonctionnalités. Pour éviter cela, [Jitsi
Meet](../communiquer/jitsi-meet.md) et Framadate, deux logiciels libres,
peuvent facilement remplacer ces solutions. Ce sont deux exemples qui
vous montrent que pour travailler ensemble, il n'est pas utile d'obliger
les collaborateur⋅rice⋅s à devenir captif⋅ve⋅s d'un service.

[![guide-libre-association](../img/guide_libre_association.jpg){: .illustration }](https://framabook.org/guide-libre-association/){target=_blank}
*[April](https://www.april.org/){target=_blank} / [Framasoft](https://framasoft.org/fr/){target=_blank}  
[Guide Libre Association](https://framabook.org/guide-libre-association/){target=_blank} -
[[PDF ↓]](http://guide.libreassociation.info/includes/guide-libre-association-framabook-version-30-decembre-2015-couleur-ecran.pdf){target=_blank} -
[[EPUB ↓]](http://guide.libreassociation.info/includes/guide-libre-association-framabook-version-30-decembre-2015.epub){target=_blank}         
Des logiciels pour libérer votre projet associatif  
Framabook 2016  
Licence [CC BY-SA](https://creativecommons.org/licenses/by-sa/3.0/fr/){target=_blank}*
{: .figure }
