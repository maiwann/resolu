---
title: Pratiques du changement
---

Quelques repères pratiques pour mener efficacement une migration vers le
Libre
{: .slogan-subtitle }

Au début d'un projet de migration vers le libre, il est important de se
poser quelques bonnes questions. L'expérience associative est
exemplaire sur bien des points. D'après quelques retours d'expérience
auprès d'association de tailles différentes, voici sous forme de
tableau quelques points d'attention à prendre en compte. Évidemment la
liste n'est pas exhaustive.

----------------------------------------------------------------------------------------- ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

| Initiative                                                                             | Stratégie |
| -------------------------------------------------------------------------------------- | --------- |
|Qui mène l'initiative ?                                                                 |  Sera-t-elle menée jusqu'au bout ? Mieux vaut éviter de faire reposer les décisions et les responsabilités sur un⋅e seul⋅e chef⋅fe de projet. Mieux vaut une ou deux personnes coordinatrices et un ensemble d'initiatives collectives au plus près des problèmes. |
| À quels degrés les personnes utilisatrices sont-elles impliqué⋅e⋅s dans les décisions ? |   Les facteurs d'adoption ou d'inhibition dans l'adoption du Libre sont autant collectifs qu'individuels. |
| Quand annoncer le projet ?                                                              |   Prenez le temps de mûrir le projet pour pouvoir l'exposer, l'expliquer et donner toute les bonnes informations pour assurer une collaboration horizontale de qualité. |
| Quand impliquer les utilisateur⋅rice⋅s ?                                                |  Il⋅elle⋅s peuvent être impliqué⋅e⋅s à différentes étapes, il n'est pas nécessaire de les solliciter à tout bout de champ. |
| Prévoir les périodes.                                                                   |   Adopter un nouveau logiciel ne se fait pas en période de « coup de bourre » : organisez, planifiez et adaptez le calendrier en fonction de la réalité. |

----------------------------------------------------------------------------------------- ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

## Migration : contraintes collectives

|  Facteur                                                                  |  Action à mener |
| ------------------------------------------------------------------------- | --------------- |
| Besoins de formation aux logiciels et aux usages (manque de temps, organisation des formations, etc.)                                                                       | Anticiper les niveaux d'expertise suffisant des utilisateur⋅rice⋅s, planifier l'adoption des logiciels en fonction du temps consacré à la formation, profiter d'une période calme dans les activités courantes de l'association. |
| Interopérabilité (compatibilité entre systèmes, gestion des formats pour les archives, etc.)    |    L'adoption d'un logiciel libre n'est pas qu'une question technique : il faut évaluer toute l'organisation de la production d'information de manière à minimiser les risques de conflits technologiques, évaluer les pertes acceptables d'informations\... |
| Information insuffisante (les procédures deviennent inefficaces).         | Prendre le temps d'informer les personnes utilisatrices (ce qui suppose une expertise suffisante), expliquer les nouvelles procédures pas à pas, personnaliser les explications. |
| Disparités logicielles entre tou⋅te⋅s les membres de l'association.       |  Insister sur la notion d'interopérabilité mais laisser leurs libertés de choix aux utilisateur⋅rice⋅s. Par ex.: on peut utiliser des formats ouverts pour échanger des documents sans pour autant obliger chacun⋅e à utiliser GNU /Linux comme système d'exploitation. Utiliser des services en ligne permet aussi de décorréler les contenus des logiciels installés localement. |
| Manque de clarté sur les évolutions futures du logiciel libre (lorsqu'elles dépendent souvent d'une poignée ou même d'un⋅e seul⋅e développeur⋅se).                                                            |  Identifier correctement les besoins et les évaluer par rapports aux constantes du logiciel envisagé (se renseigner auprès d'une autre association ayant fait la même démarche, envisager les futures mises à jour, éventuelle implication de votre association dans le développement du logiciel...). |

------------------------------------------------------------------------------------------------------------------------------------------------------- -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

## Migration : contraintes du point de vue utilisateur⋅rice

| Facteur   | Action à mener |
| --------- | -------------- |
| Insuffisances fonctionnelles (constatées factuellement ou seulement ressenties en raison d'un manque d'expertise de l'utilisateur⋅rice). |  Avant de se demander s'il faut choisir un autre logiciel (et donc perdre du temps et de la productivité), il faut impliquer les personnes utilisatrices dans le choix : désigner des « beta-testeur⋅se⋅s », planifier une période réservée à l'expérimentation. |
| Interopérabilité (compatibilité entre systèmes, gestion des formats pour les archives, etc.) | Expliquer ces notions souvent abstraites : elles font appel à de nouveaux usages et degrés d'empathie. Apprendre à gérer l'apparition d'un nouveau format dans le flux d'information. |
| Différences ergonomiques ou logiques d'usages, aptitudes (« je n'arrive pas à faire ce que je faisais auparavant, donc ce logiciel est mauvais »). | Remettre en question les pratiques, y compris les anciennes pratiques de contournement des contraintes : élaborer un plan de communication et d'apprentissages techniques collectifs, ne jamais laisser un⋅e membre seul⋅e face à ses difficultés techniques. |
| Information insuffisante (« je ne sais pas si je peux procéder ainsi ») ou trop directives (« on ne me fait pas confiance »).   | Élaborer un plan de communication et d'échanges collectifs, ne jamais laisser un⋅e membre seul⋅e face à ses doutes, confirmer concrètement la légitimité de chacun⋅e à donner son avis (créer des espaces et des temps d'échange pour cela). |
| Solitude du⋅de la membre bénévole à domicile (comment adopter un logiciel libre ?) | Proposer des évènements collectifs dédiés au logiciel libre, favoriser l'entraide entre les membres, créer une équipe « support » (en présentiel, par téléphone, par vidéo conférence, ou en ouvrant une session à distance sur la machine). |
| La responsabilité des échecs repose sur un⋅e ou quelques membres bénévoles (sentiment de se lancer dans une croisade perdue d'avance) | Les choix stratégiques doivent toujours être collectifs et ne reposent pas sur une seule personne qui endosse la responsabilité de ces choix. |
| Les discours culpabilisants face aux non-utilisateur⋅rice⋅s de logiciels libres (tenus co-responsables du manque d'éthique des logiciels qu'ils ou elles utilisent, ou des menaces sur les libertés numériques, les atteintes à la vie privée). | Éviter les phénomènes de réactance (déni, recherche de justifications erronées, hostilité). Tenir un discours d'ouverture, non catastrophiste et empathique. |

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

## Migration : facteurs d'adoption (du point de vue collectif)

| Facteur | Points de vigilance |
| ------- | ------------------- |
| Indépendance technologique de la structure par rapport aux contraintes (financières, éthiques) des logiciels privateurs. | Estimer les nouvelles contraintes de cette autonomie : compétences, frais de fournisseurs externes (p. ex. location de serveur) |
| Les formations ou les changements dans l'infrastructure sont vécus comme des investissements. | Tout retour en arrière aura un coût très élevé, il faut donc tout faire pour l'éviter. Les nouvelles compétences des utilisateur⋅rice⋅s doivent être valorisées et utilisées et surtout pas oubliées ou négligées (sinon, « à quoi bon ? ») |
| Accueil positif d'une stratégie sur le long terme, sentiment d'implication des personnes utilisatrices. | Débordements enthousiastes possibles, initiatives risquées. |
| Implication des personnes utilisatrices (groupe) dans la prise de décision (défi collectif). | Savoir organiser les réunions, modérer les débats. |
| Les membres bénévoles se forment mutuellement aux usages. | Comment organiser le temps de travail pour laisser la place à ces séquences d'entraide. |
| Passer du statut de personne utilisatrice à celui de personne utilisatrice-contributrice (action : contribuer au développement du logiciel (même symboliquement), programmation, traductions, dons\...) | Du temps de travail disponible, proposer au vote le déblocage d'une somme à donner aux développeur⋅se⋅s, promouvoir le logiciel\...
| Interopérabilité (« enfin, on se comprend »). | Savoir gérer l'interopérabilité ou l'absence d'interopérabilité dans les relations externes à l'association. |

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

## Migration : facteurs d'adoption (du point de vue utilisateur⋅rice)

| Facteur | Points de vigilance |
| ------- | ------------------- |
| Utilisation de logiciel libre dans la sphère privée. | Toutes les pratiques individuelles ne sont pas forcément les mieux appropriées dans le milieu professionnel. |
| Facilité d'utilisation, ergonomie. | Une mauvaise ergonomie doit faire l'objet d'une grande attention, car l'efficacité d'un logiciel ne suffit pas toujours à palier son design insuffisant : c'est un facteur de risque. |
| Interopérabilité : moins de stress\... | Décider quels sont les formats appropriés à votre association. |
| Valorisation de l'utilisateur·rice (on demande son expertise et son avis). | Être à l'écoute des « remontées utilisateur·rice·s »
| Compréhension des avantages pour son travail et son association. | Entretenir l'effort collectif. |
| Passer du statut de personne utilisatrice à celui de personne utilisatrice-contributrice (action : contribuer au développement du logiciel (même symboliquement). programmation, traductions, dons\... | Réserver éventuellement du temps de travail à cette tâche.   Les actions doivent être décidées collectivement. |
| Nouveaux apprentissages. | Réserver du temps de formation, valoriser les acquis. |
