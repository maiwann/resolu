---
title: Gérez votre comptabilité avec Garradin
---

Outil de gestion comptable d'association de petite et moyenne taille
{: .slogan-subtitle }

Gérer les caisses de son association prend parfois beaucoup de temps et
d'énergie. Que vous soyez actuellement noyé⋅e⋅s dans une multitude de
feuilles de calcul, en recherche d'outils et de méthodes, ou abonné⋅e à
un service trop cher et propriétaire, une solution alternative est
possible pour remplir efficacement vos obligations comptables.

Garradin est un outil complet et simple d'utilisation. Pas besoin
d'être comptable pour s'en servir ; il est conçu pour faciliter les
tâches de comptabilité associative. Grâce à ses nombreuses
fonctionnalités et à sa communauté active d'utilisateur⋅rice⋅s,
Garradin répond au besoin des petites et moyennes structures. Essayez et
voyez par vous-mêmes ; c'est ainsi que chacun⋅e y trouve son compte !

### Toute la gestion comptable en un logiciel

Le premier atout de Garradin est de rendre la comptabilité accessible à
tou⋅te⋅s, tout en permettant une gestion complète, à même de satisfaire
un⋅e véritable comptable. On dispose d'un outil de comptabilité
intuitif qui, par exemple, rend accessibles la comptabilité à
double-entrée et le plan comptable associatif. Garradin comprend la
production des résultats comptables et des bilans obligatoires pour les
assemblées générales et les demandes de subventions. Enfin, des
représentations graphiques rendent compte du budget de l'association
pour un suivi au jour le jour et en un coup d'œil.

La regroupement de toutes les données comptables et relatives aux
adhérent⋅e⋅s (notamment les cotisations) en un seul endroit est un autre
avantage : on sécurise ainsi ses données et ses archives, sans risque de
confusion avec des versions obsolètes.

### Vous en voulez plus ?

Gagnez du temps en laissant chaque adhérent⋅e s'inscrire et mettre à
jour ses informations personnelles. Besoin de récupérer l'ensemble de
ces données ? L'exportation en CSV vous facilite la tâche -- surtout si
vous profitez du nombre illimité d'inscriptions.

Encouragez les initiatives et l'intelligence collective pour
capitaliser toutes les informations utiles dans un wiki interne
collaboratif. Pour donner davantage de visibilité à vos actions,
choisissez les pages qui apparaîtront sur votre site web public ! Vous
gardez la main sur les droits d'accès et de modification.

Enfin, comme Garradin est un logiciel libre, il vous est possible de
(demander à quelqu'un de) développer une extension afin de répondre aux
éventuels besoins spécifiques de votre association. Si vous pensez
qu'elle peut être utile à d'autres organisations, vous pourrez
l'ajouter aux extensions déjà partagées par la communauté Garradin !

### Compter en liberté

Les données comptables de votre association et les données personnelles
de vos adhérent⋅e⋅s sont des données critiques. Mieux vaut être en
mesure de choisir les modalités de stockage, de traitement,
d'utilisation et de diffusion de ces données.

Utiliser un logiciel propriétaire pour la gestion comptable de son
association implique de faire confiance à l'organisation éditrice de ce
logiciel vis-à-vis de ces modalités. Le code n'étant pas accessible, il
est impossible de savoir comment sont gérées les données confiées.

À l'inverse, utiliser un logiciel libre comme Garradin vous assure un
plus haut degré de transparence :

-   Si vous choisissez d'installer Garradin sur votre propre serveur,
    la transparence est totale : le code qui traite vos données est
    ouvert, et vos données restent sur votre serveur.

-   Si vous choisissez d'utiliser Garradin sans l'installer, le code
    est toujours ouvert, et vous faites confiance à l'organisation qui
    héberge vos données.

Quelle que soit l'option que vous choisissez, vous restez
indépendant⋅e : les possibilités d'importation et d'exportation de
données dans un format libre (CSV) vous permet de migrer vers une autre
installation de Garradin, ou d'utiliser un autre logiciel.

La gestion collaborative de ces données et du wiki apporte une autre
dimension éthique : celle d'une gouvernance plus horizontale et
transparente de votre association. Une gestion fine des droits d'accès
vous permet toujours de choisir les modalités de cette collaboration.

La possibilité donnée aux adhérent⋅e⋅s de consulter directement les
données comptables sur Garradin a aussi un impact écologique : vous
évitez les multiples envois de mails contenant des fichiers lourds et
gourmands en ressources énergétiques.

Enfin, Garradin est un logiciel gratuit, maintenu et amélioré grâce aux
contributions de ses utilisateur⋅rice⋅s. Si vous souhaitez soutenir ce
modèle, vous pouvez faire un don à l'association Kidideux, qui héberge
Garradin et en assure la pérennité.

### 3 façons possibles de commencer aujourd'hui

-   Essayer Garradin sans rien installer en créant un compte pour votre
    association depuis le site officiel
    ([garradin.eu](http://garradin.eu/)).

-   Installer Garradin sur votre propre serveur (en suivant les
    instructions présentes dans la documentation)

-   Utiliser votre Garradin, inclus dans la suite de logiciels
    [Zourit]{.renvoi-fiche-collaborer}.

Une communauté s'active pour vous accompagner : plus de 2000
associations utilisent Garradin, et plusieurs espaces de discussions et
de documents sont disponibles.
